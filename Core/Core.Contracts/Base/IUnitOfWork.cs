﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Core.Contracts
{
    public interface IUnitOfWork
    {
        IUserRepository Users { get; set; }
        IPropertyRepository Properties{ get; set; }

        Task Complete();
    }
}
