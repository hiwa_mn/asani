﻿using Core.Contracts;
using Core.Entities;
using Core.Entities.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.ApplicationServices
{
    public class DeleteProperty : IDeleteProperty
    {
        private readonly IUnitOfWork unit;

        public DeleteProperty(IUnitOfWork unit)
        {
            this.unit = unit;
        }

        public async Task<Property> Execute(Guid Id)
        {
            var @property = await unit.Properties.GetAsync(Id);
            if (@property != null)
            {
                unit.Properties.Remove(@property);
                await unit.Complete();
            }
            return @property;
        }
    }
}
