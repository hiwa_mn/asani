﻿
using System;
using System.Collections.Generic;

namespace Core.Entities.Dto
{
    public class BaseApiResult
    {
        public bool Status { get; set; }
        public string Message { get; set; }
    }
    public class BaseApiPageResult : BaseApiResult
    {
        public PageDto Page { get; set; }
    }

    public class ApiResult : BaseApiResult
    {
        public object Object { get; set; }
    }
    public class PageDto
    {
        public int Total { get; set; }
        public int CurrentCount { get; set; }
        public int PageNo { get; set; }
    }
    public class FixedIntDto
    {
        public int? Id { get; set; }
        public string Name { get; set; }
    }
    public class FixedGuidDto
    {
        public Guid? Id { get; set; }
        public string Name { get; set; }
    }


}
