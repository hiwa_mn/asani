﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entities
{
    public class Property : UserBaseEntity
    {
        public int No { get; set; }
        public string Name { get; set; }
        public int Area { get; set; }
        public string Address { get; set; }
        public bool IsNorthern { get; set; }

    }
}
