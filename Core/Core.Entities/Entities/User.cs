﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entities
{
    public class User : BaseEntity
    {
        public string Name { get; set; }
        public string FamilyName { get; set; }
        public string Mobile { get; set; }
        public ICollection<Property> Properties { get; set; }

    }
}
