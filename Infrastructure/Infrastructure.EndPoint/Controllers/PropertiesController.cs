﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Core.Entities;
using Infrastructure.EF;
using Core.Contracts;
using Core.ApplicationServices;

namespace Infrastructure.EndPoint.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PropertiesController : ControllerBase
    {
        private readonly IUnitOfWork unit;
        private readonly IDeleteProperty deleteProperty;

        public PropertiesController(IUnitOfWork unit, IDeleteProperty deleteProperty)
        {
            this.unit = unit;
            this.deleteProperty = deleteProperty;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Property>>> GetProperties()
        {
            return await unit.Properties.GetAllAsync();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Property>> GetProperty(Guid id)
        {
            return await unit.Properties.GetAsync(id);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> PutProperty(Guid id, Property @property)
        {
            if (id != @property.Id)
            {
                return BadRequest();
            }
            unit.Properties.ChangeState(property);
            await unit.Complete();


            return NoContent();
        }

        [HttpPost]
        public async Task<ActionResult<Property>> PostProperty(Property @property)
        {
            await unit.Properties.AddAsync(property);
            await unit.Complete();
            return CreatedAtAction("GetProperty", new { id = @property.Id }, @property);
        }


        //An example which shows that how I write my use cases 
        /// ////////////////////////////////////////////////////////////////////////////////////////
        [HttpDelete("{id}")]
        public async Task<ActionResult<Property>> DeleteProperty(Guid id)
        {
            return await deleteProperty.Execute(id);
        }

        private bool PropertyExists(Guid id)
        {
            return unit.Properties.Any(p => p.Id == id);
        }
    }
}
