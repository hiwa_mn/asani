﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Infrastructure.EndPoint.Migrations
{
    public partial class _28031 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedAt = table.Column<long>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    FamilyName = table.Column<string>(nullable: false),
                    Mobile = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Properties",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    UserId = table.Column<Guid>(nullable: false),
                    CreatedAt = table.Column<long>(nullable: false),
                    No = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true),
                    Area = table.Column<int>(nullable: false),
                    Address = table.Column<string>(nullable: false),
                    IsNorthern = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Properties", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Properties_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "CreatedAt", "FamilyName", "Mobile", "Name" },
                values: new object[,]
                {
                    { new Guid("462b27c3-f52d-4188-92c4-6621bbbb8e40"), 1592425504L, "فامیلی کاربر 1", "09121111111", "کاربر 1" },
                    { new Guid("efd7d3f5-24a1-491b-93f9-3a25fe1186e7"), 1592425504L, "فامیلی کاربر 2", "09122222222", "کاربر 2" },
                    { new Guid("c904b078-29cd-4744-b6b4-aef97bdd4fcf"), 1592425504L, "فامیلی کاربر 3", "09123333333", "کاربر 3" },
                    { new Guid("ad658507-f324-4720-b08f-bc7b3c7e22f8"), 1592425504L, "فامیلی کاربر 4", "09124444444", "کاربر 4" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Properties_UserId",
                table: "Properties",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Properties");

            migrationBuilder.DropTable(
                name: "Users");
        }
    }
}
