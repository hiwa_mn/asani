﻿using Core.ApplicationServices;
using Core.Contracts;
using Core.Entities;
using Core.Entities.GlobalSettings;
using Domain.Contract;
using Infrastructure.EF.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using Utility.Tools.Auth;
using Utility.Tools.General;
using Utility.Tools.Notification;
using Utility.Tools.SMS.Rahyab;
using Utility.Tools.Swager;
using static Microsoft.AspNetCore.Hosting.Internal.HostingApplication;

namespace Infrastructure.EF
{
    public static class Extension
    {
        public static void AddApplicationServices(this IServiceCollection services)
        {
            var applicationServiceType = typeof(IApplicationService).Assembly;
            var AllApplicationServices = applicationServiceType.ExportedTypes
               .Where(x => x.IsClass && x.IsPublic && x.FullName.Contains("ApplicationService")).ToList();
            foreach (var type in AllApplicationServices)
            {
                services.AddScoped(type.GetInterface($"I{type.Name}"), type);
            }
        }
        public static void AddRepositories(this IServiceCollection services)
        {
            var repositpryType = typeof(Repository<>).Assembly;
            var AllRepositories = repositpryType.ExportedTypes
               .Where(x => x.IsClass && x.IsPublic && x.Name.Contains("Repository") && !x.Name.StartsWith("Repository")).ToList();
            foreach (var type in AllRepositories)
            {
                services.AddScoped(type.GetInterface($"I{type.Name}"), type);
            }
        }
        public static void ConfigureServices(this IServiceCollection services, Microsoft.Extensions.Configuration.IConfiguration configuration)
        {
            services.AddApplicationServices();
            services.AddRepositories();            
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<INotification, RahyabService>();
        }


        public static void AddEntityConfiguration(this ModelBuilder builder)
        {
            var typesToRegister = Assembly.GetExecutingAssembly().GetTypes()
                         .Where(t => t.GetInterfaces().Any(gi => gi.IsGenericType && gi.GetGenericTypeDefinition() == typeof(IEntityTypeConfiguration<>))).ToList();

            foreach (var type in typesToRegister)
            {
                dynamic configurationInstance = Activator.CreateInstance(type);
                builder.ApplyConfiguration(configurationInstance);
            }
        }
        public static void SeedData(this ModelBuilder builder)
        {

            builder.Entity<User>().HasData(new User {Id = Guid.Parse("462b27c3-f52d-4188-92c4-6621bbbb8e40"), CreatedAt = Agent.Now,Name = "کاربر 1",FamilyName = "فامیلی کاربر 1",Mobile = "09121111111"});
            builder.Entity<User>().HasData(new User {Id = Guid.Parse("efd7d3f5-24a1-491b-93f9-3a25fe1186e7"), CreatedAt = Agent.Now,Name = "کاربر 2",FamilyName = "فامیلی کاربر 2",Mobile = "09122222222"});
            builder.Entity<User>().HasData(new User {Id = Guid.Parse("c904b078-29cd-4744-b6b4-aef97bdd4fcf"), CreatedAt = Agent.Now,Name = "کاربر 3",FamilyName = "فامیلی کاربر 3",Mobile = "09123333333"});
            builder.Entity<User>().HasData(new User {Id = Guid.Parse("ad658507-f324-4720-b08f-bc7b3c7e22f8"), CreatedAt = Agent.Now,Name = "کاربر 4",FamilyName = "فامیلی کاربر 4",Mobile = "09124444444"});
                    
        }
    }
}
