﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.EF.Configuration
{
    public class Property : IEntityTypeConfiguration<Core.Entities.Property>
    {
        public void Configure(EntityTypeBuilder<Core.Entities.Property> builder)
        {
            builder.HasKey(p => p.Id );
            builder.Property(p => p.No).ValueGeneratedOnAdd();
            builder.Property(p => p.CreatedAt).IsRequired();
            builder.Property(p => p.Address).IsRequired();
            builder.Property(p => p.Area).IsRequired();
            builder.Property(p => p.IsNorthern).IsRequired();

            builder.HasOne(p => p.User).WithMany(p=>p.Properties).HasForeignKey(p => p.UserId).OnDelete(DeleteBehavior.Restrict);
        }
    }
}