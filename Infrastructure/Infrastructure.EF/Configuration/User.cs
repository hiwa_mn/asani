﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.EF.Configuration
{
    public class User : IEntityTypeConfiguration<Core.Entities.User>
    {
        public void Configure(EntityTypeBuilder<Core.Entities.User> builder)
        {
            builder.HasKey(p => new { p.Id });
            builder.Property(p => p.CreatedAt).IsRequired();
            builder.Property(p => p.Name).IsRequired();
            builder.Property(p => p.FamilyName).IsRequired();
            builder.Property(p => p.Mobile).IsRequired();
        }
    }
}