﻿using Core.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Linq;
using System.Reflection;

namespace Infrastructure.EF
{
    public class MainContext : DbContext
    {


        public DbSet<User> Users { get; set; }
        public DbSet<Property> Properties{ get; set; }

        public MainContext(DbContextOptions<MainContext> options) : base(options)
        {

        }
        public MainContext()
        {
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.AddEntityConfiguration();
            builder.SeedData();
        }

    }
}
