﻿using Core.Contracts;
using Infrastructure.EF;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Contract
{
    public class UnitOfWork : IUnitOfWork
    {
        public UnitOfWork(
            MainContext ctx,
        IUserRepository Users,
        IPropertyRepository Properties
            )
        {
            this.ctx = ctx;
            this.Users = Users;
            this.Properties = Properties;
        }
        public MainContext ctx { get; set; }
        public IUserRepository Users { get; set; }
        public IPropertyRepository Properties { get; set; }


        public async Task Complete()
        {
            await ctx.SaveChangesAsync();
        }
    }
}
