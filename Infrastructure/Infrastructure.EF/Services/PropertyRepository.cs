﻿using Core.Contracts;
using Core.Entities;
using Core.Entities.Dto;
using Domain.Contract;
using Enums;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Infrastructure.EF.Services
{
    public class PropertyRepository : Repository<Property>, IPropertyRepository
    {
        private readonly MainContext ctx;

        public PropertyRepository(MainContext ctx) : base(ctx as DbContext)
        {
            this.ctx = ctx;
        }

        
    }
}
