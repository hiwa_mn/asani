﻿using Core.Contracts;
using Core.Entities;
using Core.Entities.Dto;
using Core.Entities.Functions;
using Core.Entities.GlobalSettings;
using Domain.Contract;
using Enums;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Infrastructure.EF.Services
{
    public class UserRepository : Repository<User>, IUserRepository
    {
        private readonly MainContext ctx;

        public UserRepository(MainContext ctx) : base(ctx as DbContext)
        {
            this.ctx = ctx;
        }
    }
}
