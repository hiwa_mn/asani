﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;

namespace Utility.Tools.General
{
    public class Agent
    {
        public static string JSONSerialize(object obj)
        {
            string result = JsonConvert.SerializeObject(obj);
            result = result.TrimStart('\"');
            result = result.TrimEnd('\"');
            result = result.Replace("\\", "");
            return result;
        }
        public static string GetTdButton(string Text,string Controller,string Action,string Id,string Class)
        {
           
            string result = $"<td><input type=\"button\" class=\"{ Class}\" value=\"{Text}\" onclick=getAddress('{Controller}','{Action}','{Id}')></td>";
            return result;
        }

        public static long Now => DateTime.Now.ToUnix();
        public static void InsertLog(string message)
        {
            try
            {
                string path = AppDomain.CurrentDomain.BaseDirectory + @"Logs\NikoService.txt";
                string pd = FreeControls.PersianDate.Now.ToString();
                string msg = pd + '\t' + message;
                if (!File.Exists(path))
                    using (StreamWriter sw = File.CreateText(path))
                    {
                        sw.WriteLine(msg);
                    }
                else
                    using (StreamWriter sw = File.AppendText(path))
                    {
                        sw.WriteLine(msg);
                    }
            }
            catch (Exception)
            {
            }
        }
        public static long UnixTimeNow()
        {
            var timeSpan = (DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0));
            return (long)timeSpan.TotalSeconds;
        }


        public static void SaveFile(byte[] file, string address)
        {
            FileStream sw = new FileStream(address, FileMode.Create);
            byte[] Bytes = file;
            sw.Write(Bytes, 0, Bytes.Length);
            sw.Close();
        }

        public static List<T> CastObjToList<T>(T Obj)
        {
            if (Obj != null)
            {
                List<T> temp = new List<T>();
                temp.Add(Obj);
                return temp;
            }
            return new List<T>();
        }

        public static void MapAllFields(object source, object dst,string FieldName)
        {
            var ps = source.GetType().GetProperties();
            foreach (var item in ps)
            {
                if (item.Name != FieldName)
                {
                    var o = item.GetValue(source);
                    var p = dst.GetType().GetProperty(item.Name);
                    if (p != null)
                    {
                        if (o != null)
                        {
                            try
                            {
                                Type t = Nullable.GetUnderlyingType(p.PropertyType) ?? p.PropertyType;
                                object safeValue = (o == null) ? null : o;// Convert.ChangeType(o, t);
                                p.SetValue(dst, safeValue);
                            }
                            catch { }
                        }
                    }
                }
            }
        }

            

        public static long UnixOfTime(DateTime dateTime)
        {
            var timeSpan = (dateTime - new DateTime(1970, 1, 1, 0, 0, 0));
            return (long)timeSpan.TotalSeconds;
        }


        public static DateTime GetToday(int Plus)
        {
            var now = DateTime.Now;
            return new DateTime(now.Year, now.Month, now.Day, 0, 0, 1).AddDays(Plus);
        }


        public static Type GetTables(string Name)
        {
            return Assembly.GetExecutingAssembly().GetTypes().FirstOrDefault(t=> t.IsClass & t.Name == Name);                           
        }

        public static void FileLogger(string text)
        {
            using (StreamWriter outputFile = new StreamWriter( "Log.txt",true))
            {               
                    outputFile.WriteLine(DateTime.Now.ToString() + " : " +  text);
            }
        }

        public static T FromJson<T>(string str)
        {
            return JsonConvert.DeserializeObject<T>(str);
        }
        public static string GenerateCode()
        {
            int _min = 1000;
            int _max = 9999;
            Random _rdm = new Random();
            return _rdm.Next(_min, _max).ToString();
        }

        public static string GenerateRandomNo(int Count)
        {
            int _min = (int)Math.Pow(10,Count-1);
            int _max = (int)Math.Pow(10, Count)-1;
            Random _rdm = new Random();
            return _rdm.Next(_min, _max).ToString().PadLeft(5, '0');
        }


        public static string GetError(Exception exception)
        {
            if (exception.Message.Contains("NikoErrorText"))
                return MakeError(exception.Message);
            try
            {
                StackTrace trace = new StackTrace(exception, true);
                var frame = trace.GetFrame(2);


                string text = exception.StackTrace;
                string Line = text.Substring(text.IndexOf("ApplicationServices"));
                Line = Line.Substring(0, Line.IndexOf(" at"));
                string fileName = Line.Split(':')[0];
                Line = Line.Split(':')[1].Replace("line", "").Replace("\r\n", "").Trim();

                Line = frame.GetFileLineNumber().ToString();
                fileName = frame.GetFileName();

                var result = CreateException(exception);

                return MakeError(result);
            }
            catch
            {
                return MakeError(ToJson(new { NikoError = exception.Message, NikoErrorText = "خطا", NikoErrorCode = "2" }));
            }
        }

        public static string CreateException(Exception ex)

        {

            System.Diagnostics.StackTrace trace = new System.Diagnostics.StackTrace(ex, true);

            string diagnoseResults = "";

            for (int i = 0; i < trace.FrameCount; i++)

            {

                diagnoseResults += i.ToString() + ":" + trace.GetFrame(i).GetMethod().Name + ":" + trace.GetFrame(i).GetFileLineNumber() + ":" + trace.GetFrame(0).GetFileColumnNumber() + "-";

            }

            string[] diagnoseBlocks = diagnoseResults.Split(new char[] { '-' });

            string processesException = "";

            processesException += "Exception Code = " + "12" + "\r\n";

            processesException += "Exception Message = " + "dddd" + "\r\n";

            processesException += "Exception Class = " + ex.TargetSite.ReflectedType.Name + "\r\n\r\n";

            processesException += "Exception Path:\r\n";

            processesException += "===============\r\n";

            for (int i = 0; i < diagnoseBlocks.Length - 1; i++)

            {

                string[] diagnoseCell = diagnoseBlocks[i].Split(new char[] { ':' });

                processesException += "Exception Level = " + i.ToString() + "\r\n";

                processesException += "Exception Method = " + diagnoseCell[1].ToString() + "\r\n";

                processesException += "Exception Line number = " + diagnoseCell[2].ToString() + "\r\n";

                processesException += "Exception Column number = " + diagnoseCell[3].ToString() + "\r\n\r\n";

            }

            return "$" + processesException + "$";

        }



        public static string MakeResponse(string Response)
        {
            return ToJson(new { Response });
        }

        public static string MakeError(string Response)
        {
            return ToJson(new { Error = Response });
        }

        public static string ToJson(object Obj)
        {
            return JsonConvert.SerializeObject(Obj, Formatting.None,
                        new JsonSerializerSettings()
                        {
                            ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                            NullValueHandling = NullValueHandling.Ignore
                            //,Error = Han
                        });
        }


        public static string Encrypt(string text)
        {
            return DES.Encrypt(text, "AArIFHhOAi2fqmA/FCr6TtQKYgFO5je+");
        }
        public static string Decrypt(string text)
        {
            return DES.Decrypt(text, "AArIFHhOAi2fqmA/FCr6TtQKYgFO5je+");
        }

        public static string EncryptRsa(string text, string PublicKey)
        {
            Agent.FileLogger("Encrypt: " + PublicKey);
            return Hashing.GetPacket(text, PublicKey);
        }
        public static string DecryptRsa(string text, string Key)
        {
            return Hashing.GetFields(text, Key);
        }


        
    }
}
