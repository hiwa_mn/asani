﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Utility.Tools.General;

namespace Utility.Tools
{
    public class FireBase : IFireBase
    {
        private readonly IConfiguration configuration;

        public FireBase(IConfiguration configuration)
        {
            this.configuration = configuration;
        }
        public string SendNotification(List<NotificationObject> not)
        {
            var result = "";
            List<object> list = new List<object>();
            list.Add(not);
            Task myTask = Task.Run(() => result = Run(list));
            myTask.Wait();
            return result;
        }

        public string Run(List<object> arg)
        {
            string resultStr = "";
            configuration.GetSection<FireBaseSetting>();
            List<NotificationObject> not = arg[0] as List<NotificationObject>;

            foreach (var item in not)
            {
                WebRequest tRequest = WebRequest.Create(FireBaseSetting.FireBaseApiAddress);
                tRequest.Method = "post";
                tRequest.Headers.Add(string.Format("Authorization: key={0}", FireBaseSetting.AuthorizationKey));
                tRequest.Headers.Add(string.Format("Sender: id={0}", FireBaseSetting.Sender));
                tRequest.ContentType = "application/json";

                var payload = new
                {
                    to = item.DeviceId,
                    priority = "high",
                    isBackground = "",
                    content_available = true,
                    //notification = new  {body = item.Text,title = item.Title },
                    data = new { item.Description, item.Link, item.Image,item.FromId,item.FromName,item.Type,item.Text,item.Title},

                };
                string postbody = JsonConvert.SerializeObject(payload).ToString();
                Byte[] byteArray = Encoding.UTF8.GetBytes(postbody);
                tRequest.ContentLength = byteArray.Length;
                using (Stream dataStream = tRequest.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    using (WebResponse tResponse = tRequest.GetResponse())
                    {
                        using (Stream dataStreamResponse = tResponse.GetResponseStream())
                        {
                            if (dataStreamResponse != null)
                                using (StreamReader tReader = new StreamReader(dataStreamResponse))
                                {
                                    String sResponseFromServer = tReader.ReadToEnd();
                                    resultStr += sResponseFromServer;
                                }
                        }
                    }

                }
            }
            return resultStr;
        }

       
    }


  
}
