﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Utility.Tools
{
    public interface IFireBase
    {
        string SendNotification(List<NotificationObject> not);
        string Run(List<object> arg);
    }


    public class NotificationObject
    {
        public string DeviceId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Link { get; set; }
        public string Image { get; set; }
        public string FromId { get; set; }
        public string FromName { get; set; }
        public int Type { get; set; }
        public string Text { get; set; }
    }
}
