﻿using System.Threading.Tasks;

namespace Utility.Tools.Notification
{
    public interface INotification
    {
        Task SendAsync(string Text,string Destination);
    }
}
