﻿using Domain.Contract;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using Microsoft.Extensions.Configuration;
using Microsoft.EntityFrameworkCore;
using Core.Contracts;
using Utility.Tools.Auth;
using Infrastructure.EF;
using Utility.Tools.Notification;
using Core.ApplicationServices;
using Utility.Tools.SMS.Rahyab;
using Core.Entities.Dto;

namespace ConsoleCore
{
    class Program
    {
        public static IUnitOfWork Iunit { get; private set; }
        public static IConfiguration Configuration { get; set; }



        static async Task Main()
        {

            Config();



            Console.ReadKey();

        }



        public static void Config()
        {
            ServiceCollection service = new ServiceCollection();
            var builder = new ConfigurationBuilder().SetBasePath(@"C:\Projects\Zigorat\Sunblaze\Infrastructure\Infrastructure.EndPoint\").AddJsonFile("appsettings.json", optional: true, reloadOnChange: true).AddEnvironmentVariables();
            Configuration = builder.Build();

            service.AddSingleton<IConfiguration>(builder.Build());


            service.ConfigureServices(Configuration);
            service.AddDbContext<MainContext>(o => o.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));



            var provider = service.BuildServiceProvider();
            Iunit = provider.GetService<IUnitOfWork>();
        }

        
    }

   

 

   
}
